import face_recognition
import cv2
import numpy as np
import time
import os
import smbus

from voice_engine.kws import KWS
from voice_engine.source import Source
from voice_engine.channel_picker import ChannelPicker
from voice_engine.doa_respeaker_4mic_6ch_v2 import DOA
from gpiozero import PWMLED, LED, Robot

##cobagit
##cobagit2

####====================== Pemanggilan Compass ======================###
bus=smbus.SMBus(1)
address=0x60

def bearing255():
	bear = bus.read_byte_data(address, 1)
	return bear
	
def cmps():
	global compass
	compass = (bearing255() / 255.0) * 360
	time.sleep(0.004)
	#print "nilai kompas ",("%.1f" % bear255)



###============================ Play Sound ==========================###
def playsound():
    os.system('mpg321 google_tts.mp3 &')
    time.sleep(0.5)
    
                       

####============ Deklarasi Pin Motor ===========###
motor_a1 = LED(10)      #pin direction motor 1 CW
motor_b1 = LED(9)      	#pin direction motor 1 CCW
motor_a2 = LED(22)      #pin direction motor 2 CW
motor_b2 = LED(6)       #pin direction motor2  CCW
pwm_1 = PWMLED(5)       #pin PWM motor 1
pwm_2 = PWMLED(11)      #pin PWM motor 2
motor_e1 = LED(17)		#pin enable motor 1
motor_e2 = LED(27)		#pin enable motor 2

motor_e1.on()
motor_e2.on()



####============ Pemanggilan Arah Motor ===========###
def mundur(speed1, speed2):
    motor_a1.on()
    motor_b1.off()
    motor_a2.off()
    motor_b2.on()
    pwm_1.value = speed1
    pwm_2.value = speed2
    print ('Mundur')

def maju(speed1, speed2):
    motor_a1.off()
    motor_b1.on()
    motor_a2.on()
    motor_b2.off()
    pwm_1.value = speed1
    pwm_2.value = speed2
    print ('Maju')

def kanan(speed1, speed2):
    motor_a1.off()
    motor_b1.on()
    motor_a2.off()
    motor_b2.off()
    pwm_1.value = speed1
    pwm_2.value = speed2
    print ('Kanan')

def kiri(speed1, speed2):
    motor_a1.off()
    motor_b1.off()
    motor_a2.on()
    motor_b2.off()
    pwm_1.value = speed1
    pwm_2.value = speed2
    print ('Kiri')
    
def pkiri(speed1, speed2):
    motor_a1.on()
    motor_b1.off()
    motor_a2.on()
    motor_b2.off()
    pwm_1.value = speed1
    pwm_2.value = speed2
    print ('Putar Kiri')
    
def pkanan(speed1, speed2):
    motor_a1.off()
    motor_b1.on()
    motor_a2.off()
    motor_b2.on()
    pwm_1.value = speed1
    pwm_2.value = speed2
    print ('Putar Kanan')

def berhenti():
    motor_a1.off()
    motor_b1.off()
    motor_a2.off()
    motor_b2.off()
    pwm_1.value = 0
    pwm_2.value = 0
    print ('Berhenti')
    kamera()
    
def berhentix():
    motor_a1.on()
    motor_b1.on()
    motor_a2.on()
    motor_b2.on()
    pwm_1.value = 0
    pwm_2.value = 0
    print ('Berhenti !!!')
    
    

###=========================== Susur Kanan ==========================###
def susurkanan(mx, c0):
    '''
    ------catatan------
    mx = posisi arah suara targer
    cx = posisi target kompas
    m0 = posisi awal mic
    c0 = posisi awal kompas
    mz = selisih posisi awal mic dengan target
    '''
    mz = mx
    cx = c0 + mz
        
    if (cx > 360):
        cx = cx - 360    
        if (compass > cx):
            while (compass > cx):    
                cmps()     
                pkanan(0.2, 0.25)
                print ("ssr_kanan", "MZ", mx, "compas", compass, "CX", cx)
                
        if (compass < cx):
            while (cx > compass):
                cmps()
                pkanan(0.2, 0.25)
                print ("ssr_kanan", "MZ", mx, "compas", compass, "CX", cx)
    else:
        cx = cx
        while compass < cx:
            cmps()     
            pkanan(0.2, 0.25)
            print ("ssr_kanan", "MZ", mx, "compas", compass, "CX", cx)
            
    #return maju(0.5, 0.5)
    time.sleep(0.5)
    return berhenti()

    
###=========================== Susur Kiri ===========================###
def susurkiri(mx, c0):
    mz = 360 - mx
    cx = c0 - mz
    time.sleep(1)
    print ("ssr_kanan", "MZ", mz, "compas", compass, "CX", cx)
    
    if (cx < 0):
        cx = cx * (-1)
        while compass > cx:
            cmps()
            pkiri(0.2, 0.25)
            print ("ssr_kanan", "MZ", mz, "compas", compass, "CX", cx)
    else:
        while compass > cx:
            cmps()
            pkiri(0.2, 0.25)
            print ("ssr_kanan", "MZ", mz, "compas", compass, "CX", cx)
    
    #return maju(0.5, 0.5)
    time.sleep(0.5)
    return berhenti()



####======================= Face Recognition =======================####
def kamera():
    print ("Open Camera")
    video_capture = cv2.VideoCapture(0)
    ret = video_capture.set (3, 480)
    ret = video_capture.set (4, 240)

    adz_image = face_recognition.load_image_file("adz3.jpg")
    adz_face_encoding = face_recognition.face_encodings(adz_image)[0]

    # Create arrays of known face encodings and their names
    known_face_encodings = [adz_face_encoding]
    known_face_names = ["Adzkia"]

    # Initialize some variables
    face_locations = []
    face_encodings = []
    face_names = []
    process_this_frame = True

    is_face_detected = False
    is_first_detected = False

    while True:
        tic = time.time()
        
        # Grab a single frame of video
        ret, frame = video_capture.read()

        # Resize frame of video to 1/4 size for faster face recognition processing
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

        # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
        rgb_small_frame = small_frame[:, :, ::-1]

        # Only process every other frame of video to save time
        if process_this_frame:
            # Find all the faces and face encodings in the current frame of video
            face_locations = face_recognition.face_locations(rgb_small_frame)
            face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
            #print(face_locations)
            face_names = []
            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
                name = "Unknown"

                face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)
                if matches[best_match_index]:
                    name = known_face_names[best_match_index]
                    if (name == "Adzkia"):
                        is_face_detected = True
                        print("detect")
                    else:
                        is_face_detected = False
                        is_first_detected = False
                        
                    if is_face_detected == True and is_first_detected == False:
                        is_first_detected = True
                        playsound()
                        last_detected = name
                        
                face_names.append(name)
                        
            if len(face_encodings) == 0: 
                is_first_detected = False
                            

                

        process_this_frame = not process_this_frame

        # Display the results
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4

            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        # Display the resulting image
        cv2.imshow('Video', frame)
        
        print(1/(time.time() - tic))
        
        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()
    
    
    
####========================= KWS dan DOA =========================#####
def main():
    
    src = Source(rate=16000, frames_size = 320, channels=6)
    ch1 = ChannelPicker(channels=src.channels, pick=0)
    kws = KWS(model='/home/pi/dds/vcn/INDI.pmdl')
    doa = DOA(rate=src.rate, chunks=20)

    src.link(ch1)
    ch1.link(kws)
    src.link(doa)

    def on_detected(keyword):
        position = int(doa.get_direction())
        
        
        cmps()
        c0 = compass
        mx = position

        print('detected {} at direction {}'.format(keyword, position))
        '''
        if (position > 0):
            print ("ACIVE")
            kamera()
        '''
        if (position > 0) and (position <= 180):
            print ("masuk kanan")
            time.sleep(2)
            susurkanan(mx, c0)
        elif (position > 180) and (position <= 360):
            print ("masuk kiri")
            time.sleep(2)
            susurkiri(mx, c0)
        else:
            maju(0, 0)
        
        
    kws.set_callback(on_detected)
    kws.start()
    src.start()
    #src.recursive_start()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
    #src.recursive_stop()
    kws.stop()
    src.stop()


if __name__ == '__main__':
    main()


